package collection.tsio;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GestionnaireContacts {

	String nom;
	HashMap<Personne, ArrayList<Contact>> map = new HashMap<Personne, ArrayList<Contact>>();

	public GestionnaireContacts(String nom) {
		this.nom = nom;
	}

	public void ajouter(Personne personne, Contact contact) {
		if (map.containsKey(personne)) {
			map.get(personne).add(contact);
		} else {
			ArrayList<Contact> contactList = new ArrayList<Contact>();
			contactList.add(contact);
			map.put(personne, contactList);
		}
	}

	public List<Contact> contacts(Personne personne) {
		return map.get(personne);
	}
	
	public String getNom(){
		return this.nom;
	}
	
	
}
