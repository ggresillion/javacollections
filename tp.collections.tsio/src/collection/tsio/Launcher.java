package collection.tsio;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class Launcher {

	public static void main(String[] args) {

		ArrayList<Personne> personnes = new ArrayList<Personne>();
		personnes.add(new Personne("Kilian", "Jaques",
				new Calendar.Builder().setCalendarType("iso8601").setDate(2010, 1, 8).build()));
		personnes.add(new Personne("Kevin", "Pascal",
				new Calendar.Builder().setCalendarType("iso8601").setDate(1995, 1, 7).build()));
		personnes.add(new Personne("Jean", "Dupont",
				new Calendar.Builder().setCalendarType("iso8601").setDate(1983, 1, 6).build()));
		Collections.sort(personnes);
		afficherPersonnes(personnes);

		System.out.println("Age moyen : " + ageMoyen(personnes)+"\n--------------------------------");
		
		Set<Personne> tree = new TreeSet<Personne>(new ComparatorPersonneAnniversaire());
		tree.addAll(personnes);
		afficherPersonnes(tree);
		
		System.out.println("Age inférieur à 18 : ");
		afficherPersonnes(ageInferieur(personnes, 18));
		
		GestionnaireContacts gestio = new GestionnaireContacts("gestionnaire");
		gestio.ajouter(personnes.get(0), new Mail("jean", "gmail.com"));
		
		
		
	}

	public static void afficherPersonnes(Collection<Personne> personnes) {

		Iterator<Personne> iterator = personnes.iterator();
		while (iterator.hasNext()) {
			System.out.println(iterator.next().toString());
		}
		System.out.println("--------------------------------");

	}

	public static float ageMoyen(Collection<Personne> personnes) {

		Iterator<Personne> iterator = personnes.iterator();
		float moyenne = 0;
		while (iterator.hasNext()) {
			moyenne += age(iterator.next());
		}
		return moyenne / personnes.size();
	}

	public static float age(Personne personne) {

		LocalDate date = new Date(personne.anniversaire.getTimeInMillis()).toInstant().atZone(ZoneId.systemDefault())
				.toLocalDate();
		return Period.between(date, LocalDate.now()).getYears();
	}
	
	public static Set<Personne> ageInferieur(Collection<Personne> personnes, int age) {
		
		Set<Personne> setPersonnes = new HashSet<Personne>();
		for (Personne personne : personnes){
			if(age(personne)<=age){
				setPersonnes.add(personne);
			}
		}
		return setPersonnes;
	}
}
