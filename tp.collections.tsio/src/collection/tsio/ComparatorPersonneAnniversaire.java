package collection.tsio;

import java.util.Comparator;

public class ComparatorPersonneAnniversaire implements Comparator<Personne>{

	@Override
	public int compare(Personne o1, Personne o2) {
		return o1.getAnniversaire().compareTo(o2.getAnniversaire());
	}

}
